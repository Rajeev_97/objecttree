import { LightningElement, wire } from 'lwc';
import getAccounts from '@salesforce/apex/ObjectTree.getAccounts';

export default class ObjectTree extends LightningElement {
  @wire(getAccounts) accounts;
}